/* u ui ctrl ide sve sto se tice i dobija iz UI
 * u budget ctrl ide sve sto se tice same logike i kalkulacije koja se odvija iza scene
 * u controller se stavlja sve sto se tice ova dva kontrolora i pozivaju se tu funkcije iz ova dva kontrolora
 * 
 *  1. prvo definisi constructor funkcije u budgetCtrl
 *  2. onda def strukturu podataka data
 *  3. definisi event listenere za prikupljanje vrednosti
 * 
 */

/****
 * BUDGET CTRL
 * ***/
var budgetController = (function () {
    var Expense = function (id, description, value) {
        this.id = id;
        this.description = description;
        this.value = value;
        this.percentage = -1;
    };
    Expense.prototype.percentage = function () {

    };
    var Income = function (id, description, value) {
        this.id = id;
        this.description = description;
        this.value = value;
    };
    var dataStructure = {
        allItems: {
            exp: [],
            inc: [],
        },
        totals: {
            exp: 0,
            inc: 0
        },
        budget: 0,
        percentage: -1
    };
    return{
        data: function () {
            return dataStructure;
        }
    };
})();

var UIController = (function () {
    var getDoms = {
        inputType: '.add__type',
        inputDescription: '.add__description',
        inputValue: '.add__value',
        inputBtn: '.add__btn',
        incomeContainer: '.income__list',
        expensesContainer: '.expenses__list',
        budgetLabel: '.budget__value',
        incomeLabel: '.budget__income--value',
        expensesLabel: '.budget__expenses--value',
        percentageLabel: '.budget__expenses--percentage',
        container: '.container',
        expensesPercLabel: '.item__percentage',
        dateLabel: '.budget__title--month'
    };
    return{
        displayBudget: function (obj) {
            var type;
            obj.budget > 0 ? type = 'inc' : type = 'exp';

            document.querySelector(getDoms.budgetLabel).textContent = obj.budget;
            document.querySelector(getDoms.incomeLabel).textContent = obj.totalInc;
            document.querySelector(getDoms.expensesLabel).textContent = obj.totalExp;
            if (obj.percentage > 0) {
                document.querySelector(getDoms.percentageLabel).textContent = obj.percentage;
            } else {
                document.querySelector(getDoms.percentageLabel).textContent = '----';
            }
        },
        DOM: function () {
            return getDoms;
        }
    };
})();

var controller = (function (budgetCtrl, UICtrl) {

    return{
        init: function () {
            console.log('app has started');
            UICtrl.displayBudget({
                budget: 0,
                totalInc: 0,
                totalExp: 0,
                percentage: -1
            });

        }
    };
})(budgetController, UIController);
controller.init();


//testing
//inputBtn = '.add__btn';
//document.querySelector(inputBtn).addEventListener('click', function (e) {
//    inputBtn = true;
//    console.log(inputBtn);
//});


