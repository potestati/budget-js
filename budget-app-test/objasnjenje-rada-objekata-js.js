/* u ui ctrl ide sve sto se tice i dobija iz UI
 * u budget ctrl ide sve sto se tice same logike i kalkulacije koja se odvija iza scene
 * u controller se stavlja sve sto se tice ova dva kontrolora i pozivaju se tu funkcije iz ova dva kontrolora
 * 
 *  1. prvo definisi constructor funkcije u budgetCtrl
 *  2. onda def strukturu podataka data
 *  3. definisi event listenere za prikupljanje vrednosti
 * OVDE JE OBJASNJENO STA ODAKLE RADI
 */

/****
 * BUDGET CTRL
 * ***/
var budgetCtrl = (function () {
    //test
    var sum = function (a, b) {
        c = a + b;
        console.log(c);
        return c;
    };
    //Expense constructor
    var Expense = function (id, description, value) {
        this.id = id;
        this.description = description;
        this.value = value;
        this.percent = -1;
    };
    Expense.prototype.calcPercentage = function (totalIncome) {
        if (totalIncome) {
            this.percent = Math.round((this.value / totalIncome) * 100);
        } else {
            this.percent = -1;
        }
    };
    Expense.prototype.getPercent = function () {
        return this.percent;
    };

    //Income constructor
    var Income = function (id, description, value) {
        this.id = id;
        this.description = description;
        this.value = value;
    };
    //struktura unesenih podataka
    var data = {
        allItems: {
            exp: [],
            inc: []
        },
        totals: {
            exp: 0,
            inc: 0
        },
        percent: -1,
        budget: 0

    };
    return{
        //proba
        rest: function () {
            return sum;
        },
        //get data for budget from data strucure
        getData: function () {
            return{
                budget: data.budget,
                totalInc: data.totals.inc,
                totalExp: data.totals.exp,
                percentOfExp: data.percent
            };
        },
        testingApp: function () {
            console.log(data);
        }
    };
})();
/****
 * UI CTRL
 * ******/
var UICtrl = (function () {
    //selekcija DOM-a u index fajlu
    var selected = {
        inputBtn: '.add__btn',
        inputType: '.add__type',
        inputDescr: '.add__description',
        inputValue: '.add__value'

    };
    //proba
    var test = {
//        text: 'hello from uictrl'
    };
//    console.log(test.text);
    return{
        //u ovom kontroloru povlacimo 
        //sve iz obj selected a domItems u drugim objektima
        domItems: function () {
            return selected;
        },
        string: function () {
            return test;
        },
        getInputValue: function () {
            return{
                type: document.querySelector(selected.inputType).value,
                description: document.querySelector(selected.inputDescr).value,
                value: parseFloat(document.querySelector(selected.inputValue).value)
            };
        }
    };
})();
// GLOBAL APP CONTROLLER link
var controller = (function (UICtrl, bdgCtrl) {

    //PRIMER pozivanje iz bdg ctrl test
    result = bdgCtrl.rest();
//    result(5, 10);
    //PRIMER pozivanje iz ui ctrl test
    var note = UICtrl.string();
//    console.log(note.text);
    /* END OF TEST */

    var setEventListener = function () {
        var DOMElements = UICtrl.domItems();
        document.querySelector(DOMElements.inputBtn).addEventListener('click', function () {
//            console.log('hello from the set event listener');
        });
    };

    var addItem = function () {
        var input, newInput;

        input = UICtrl.getInputValue();

        if (input.description !== "" && !isNaN(input.value) && input.value > 0) {
            console.log('hello from input');

        }
    };
    return{
        init: function () {
            console.log('App has started');
            setEventListener();
        }
    };
})(UICtrl, budgetCtrl);
controller.init();